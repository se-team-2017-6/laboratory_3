#include <iostream>
#include <string>
#include <math.h>
using namespace std;
void set_time_for_tasks(int array[], int n) {
	int k = 1;
	int i = 0;
	char ch[8];
	int temp = 0;
	bool flag = false;
	while (i < 2 * n) {
		do {
			temp = 0;
			std::cout << "������� ���-�� ����� �� 1 �� 99, ������� ����� ��������� �� ���������� ������ � " << k << "  ";
			std::cin >> ch;
			for (int c = 0; ch[c] != '\0'; c++) {
				if (!isdigit(ch[c])) {
					std::cout << "������: ���������� ������ �����! ��������� ����." << endl;
					flag = true;
					break;
				}
			}
			if (flag) {
				for (int � = 0; � < strlen(ch); �++) {
					ch[�] = '\0';
				}
				flag = false;
				continue;
			}
			for (int c = 0; ch[c] != '\0'; c++) {
				temp += ((int)ch[c] - 48)* pow(10, strlen(ch) - c - 1);
				if (temp < 1 || temp > 99) {
					cout << "������ �����! ���������� ����� ������ ���� �� 1 �� 99.\n��������� ����:";
					break;
				}
			}
		} while (temp < 1 || temp > 99);
		array[i] = temp;
		i++;
		do {
			temp = 0;
			std::cout << "������� ���-�� ����� �� 1 �� 99, ������� ����� ��������� �� ���������� ������ � " << k << "  ";
			std::cin >> ch;
			for (int c = 0; ch[c] != '\0'; c++) {
				if (!isdigit(ch[c])) {
					std::cout << "������: ���������� ������ �����! ��������� ����." << endl;
					flag = true;
					break;
				}
			}
			if (flag) {
				for (int c = 0; ch[c] != '\0'; c++) {
					ch[c] = '\0';
				}
				flag = false;
				continue;
			}
			for (int c = 0; ch[c] != '\0'; c++) {
				temp += ((int)ch[c] - 48)* pow(10, strlen(ch) - c - 1);
				if (temp < 1 || temp > 99) {
					cout << "������ �����! ���������� ����� ������ ���� �� 1 �� 99.\n��������� ����:";
				}
			}
		} while (temp < 1 || temp > 99);
		array[i] = temp;
		i++;
		k++;
	}
}

bool checking_the_correctness_of_the_time_entry(int array[], int n) {
	for (int i = 0; i < n * 2; i++) {
		if (array[i] > 99 || array[i] < 1) {
			cout << "������: �������� ���������� ����� � ����� ������ ���� �� 1 �� 99!" << endl;
			return false;

		}
	}
	return true;
}

void Sum_of_all_tasks(int array[], int n) {
	double *Time_of_tasks_in_hour_format = new double[n];
	int Sum_of_Hours = 0, Sum_of_Minutes = 0, c = 0;
	for (int i = 0; i < n * 2; i++) {
		if (i == 0 || i % 2 == 0) {
			Sum_of_Hours += array[i];
		}
		else {
			Sum_of_Minutes += array[i];
		}
	}
	double sum;
	sum = Sum_of_Hours + (double)Sum_of_Minutes / 60;
	if (sum <= 40.0) {
		cout << "������� ������ �� 40-������� ������� ������" << endl;
	}
	else {
		cout << "������� �� ������ �� 40-������� ������� ������" << endl;
	}
	delete[] Time_of_tasks_in_hour_format;
}

void min_and_max_tasks(int array[] , int n) {
	int number_min = 0;
	int number_max = 0; 
	double min_task = 99;
	double max_task = 0;
	double temp = 0;
	for (int i = 0; i < 2 * n; i++) {
		if (i == 0 || i % 2 == 0) {
			temp = (double)array[i] + (double)array[i+1]/ 60;
			if (temp > max_task) {
				max_task = (double)temp;
				number_max = i / 2 + 1;
			};
			if (temp < min_task) {
				min_task = (double)temp;
				number_min = i/2 + 1;
			};
		}
	}
	cout.precision(3);
	cout << "����� �������� ������ � " << number_min << " ������ " << (double)min_task << endl;
	cout << "����� ��������������� ������ � " << number_max << " ������ " << (double)max_task << endl;
}
void main() {
	setlocale(0, "Russian");
	bool flag = false;
	char s[8];
	int Number_of_tasks = 0;
	do {
		Number_of_tasks = 0;
		cout << "������� ���������� ����� ��� ���������� �� 1 �� 1000: " << endl;
		cin >> s;
		for (int i = 0; i < strlen(s); i++) {
			if (!isdigit(s[i])) {
				cout << "������: ���������� ������ �����! ��������� ����." << endl;
				flag = true;
				break;
			}
		}
		if (flag) {
			flag = false;
			continue;
		}
		for (int i = 0; i < strlen(s); i++) {
			Number_of_tasks += (int)(s[i] - 48) * pow(10, (strlen(s) - i - 1));
			if (Number_of_tasks < 1 || Number_of_tasks > 1000) {
				cout << "������ �����! ���������� ����� ��� ���������� ������ ���� �� 1 �� 1000.\n��������� ����:";
			}
		}
	} while (Number_of_tasks < 1 || Number_of_tasks > 1000);

	int *Time_To_Tasks = new int[Number_of_tasks * 2];
	set_time_for_tasks(Time_To_Tasks, Number_of_tasks);

	while (checking_the_correctness_of_the_time_entry(Time_To_Tasks, Number_of_tasks) == false) {
		set_time_for_tasks(Time_To_Tasks, Number_of_tasks);
	}

	Sum_of_all_tasks(Time_To_Tasks, Number_of_tasks);

	min_and_max_tasks(Time_To_Tasks, Number_of_tasks);
	delete[] Time_To_Tasks;
	system("pause");
}